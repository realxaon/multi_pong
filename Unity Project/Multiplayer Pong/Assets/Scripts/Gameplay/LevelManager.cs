﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;


public class LevelManager : NetworkBehaviour {
	public Action<string> OnStateChanged = delegate {
	};

	NetworkManager m_Manager;

	ALevelState m_CurrentState = new NullLevelState();


	public ALevelState CurrentState {
		get {
			return m_CurrentState;
		}
	}


	public void SetState(ALevelState newState) {
		m_CurrentState.End();
		m_CurrentState = newState;
		m_CurrentState.Start(this);
		OnStateChanged.Invoke(m_CurrentState.Name);
	}
}
