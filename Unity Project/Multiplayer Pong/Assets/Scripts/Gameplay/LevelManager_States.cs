﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public static class LevelStateNames {
	public const string Unknown = "unknown";
	public const string Waiting = "waiting";
	public const string InGame = "in game";
	public const string Finished = "finished";
}


public abstract class ALevelState {
	public abstract string Name{ get; }


	protected LevelManager Owner {
		get {
			return m_Owner;
		}
	}


	LevelManager m_Owner;
	List<Coroutine> m_StateRoutines = new List<Coroutine>();


	public virtual void Start(LevelManager manager) {
		m_Owner = manager;
	}


	public virtual void End() {
		foreach(var coroutine in m_StateRoutines)
		{
			if(coroutine != null)
			{
				m_Owner.StopCoroutine(coroutine);
			}
		}
	}


	protected void StartCoroutine(IEnumerator routine) {
		m_StateRoutines.Add(m_Owner.StartCoroutine(routine));
	}
}


public class NullLevelState : ALevelState {
	public override string Name {
		get {
			return LevelStateNames.Unknown;
		}
	}


	public override void Start(LevelManager manager) {
		//Do nothing - prevent null reference exception
	}


	public override void End() {
		//Do nothing - prevent null reference exception
	}
}


public class WaitingLevelState : ALevelState {
	public override string Name {
		get {
			return LevelStateNames.Waiting;
		}
	}


	public override void Start(LevelManager manager) {
		base.Start(manager);
		StartCoroutine(WaitForPlayersRoutine());
	}


	IEnumerator WaitForPlayersRoutine() {
		while(NetworkManager.singleton.numPlayers < 2)
		{
			yield return null;
		}

		Owner.SetState(new InGameLevelState());
	}
}


public class InGameLevelState : ALevelState {
	public override string Name {
		get {
			return LevelStateNames.InGame;
		}
	}


	public override void Start(LevelManager manager) {
		base.Start(manager);
	}
}