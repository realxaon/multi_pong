﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class PaddleInput : NetworkBehaviour {
	[SerializeField] Paddle m_Paddle;


	void Update() {
		if(isLocalPlayer)
		{
			m_Paddle.MoveDirection = 0.0f;

			if(Input.GetKey(KeyCode.LeftArrow))
			{
				m_Paddle.MoveDirection = -1.0f;
			}
			else if(Input.GetKey(KeyCode.RightArrow))
			{
				m_Paddle.MoveDirection = 1.0f;
			}
		}
	}
}
