﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


[RequireComponent(typeof(Rigidbody2D))]
public class Paddle : NetworkBehaviour {
	public float MoveDirection = 0.0f;

	[SerializeField] float m_Speeed = 2.0f;
	[SerializeField] float m_Acceleration = 5.0f;

	Rigidbody2D m_Rigidbody2D;


	void Awake() {
		m_Rigidbody2D = GetComponent<Rigidbody2D>();
	}


	void FixedUpdate() {
		if(isLocalPlayer)
		{
			m_Rigidbody2D.velocity = Vector2.MoveTowards(
				m_Rigidbody2D.velocity,
				new Vector2(MoveDirection * m_Speeed, 0.0f),
				m_Acceleration * Time.fixedDeltaTime
			);
		}		
	}
}
