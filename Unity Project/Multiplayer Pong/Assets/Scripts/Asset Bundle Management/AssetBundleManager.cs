﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class AssetBundleManager : MonoBehaviour {
	public static AssetBundleManager Instance {
		get;
		private set;
	}


	public string m_BundleURL;

	AssetBundle bundle;


	void Awake() {
		Instance = this;
		DontDestroyOnLoad(gameObject);
	}


	IEnumerator Start() {
		using(WWW www = new WWW(m_BundleURL))
		{
			yield return www;

			if(www.error != null)
			{
				Debug.LogError("Error when downloading assed bundle: " + www.error);
			}
			else
			{
				bundle = www.assetBundle;
			}
		}

		{//TODO(Move loading first scene put of this class)
			SceneManager.LoadScene(1);
		}
	}


	/// <summary>
	/// Tries to get the prefab.
	/// </summary>
	/// <returns>The prefab. If bundle isn't downloaded or don't have prefab, returns null</returns>
	/// <param name="name">Name of the prefab.</param>
	public GameObject GetPrefab(string name) {
		GameObject prefab = null;

		bool hasBundle = (bundle != null);
		bool containsAsset = hasBundle && (bundle.Contains(name));
		if(containsAsset)
		{
			prefab = bundle.LoadAsset<GameObject>(name);
		}
		else
		{
			if(hasBundle == false)
			{
				Debug.Log("No bundle");
			}
			else
			{
				Debug.LogFormat("Bundle doesn't contain asset: {0}.", name);
			}
		}


		return prefab;
	}
}
