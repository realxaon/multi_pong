﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BundlePrefabSwitch : MonoBehaviour {
	void Awake() {
		string prefabName = name;
		var prefab = AssetBundleManager.Instance.GetPrefab(prefabName);
		if(prefab != null)
		{
			Instantiate(prefab, transform.position, transform.rotation, transform.parent);
			gameObject.SetActive(false);
		}
	}
}
