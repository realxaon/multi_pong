﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameplayUI : MonoBehaviour {
	static readonly WaitForSeconds CheckForManagerDelay = new WaitForSeconds(0.25f);

	[SerializeField] GameObject m_WaitingPanel;

	LevelManager m_Manager;


	IEnumerator Start() {
		SetState(LevelStateNames.Unknown);
		while(m_Manager == null)
		{
			m_Manager = FindObjectOfType<LevelManager>();

			if(m_Manager == null)
			{
				yield return CheckForManagerDelay;
			}
			else
			{
				m_Manager.OnStateChanged += SetState;
				SetState(m_Manager.CurrentState.Name);
			}
		}
	}


	void OnDestroy() {
		if(m_Manager != null)
		{
			m_Manager.OnStateChanged -= SetState;
		}
	}


	void SetState(string stateName) {
		bool showWaitingPanel = LevelStateNames.Waiting.Equals(stateName);
		showWaitingPanel |= LevelStateNames.Unknown.Equals(stateName);

		m_WaitingPanel.SetActive(showWaitingPanel);
	}
}
