﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class AssetBundleBuild {
	[MenuItem("Assets/Build Asset Bundle")]
	static void BuildAlAssetBundles() {
		BuildPipeline.BuildAssetBundles("Assets/Asset Bundles", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
	}
}
