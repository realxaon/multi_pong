## Synopsis

A pong clone with internet based multiplayer. Uses Asset Bundles to change apperance depending on current events.

## Guidelines

*Singleplayer*

Singleplayer game is a Multiplayer game with only one human player with server and client running on the same Player (in short it's a Host). That way we don't have to duplicate logic in NetworkBehaviour and Monobehaviour components.

*Project Architecture*

Project uses extensively Model-View-Presenter pattern where Model layer is for Logic (Game Mechanisms and UI functionality), View are Unity Engine components, Game Objects and/or Prefabs, and Presenter is (to put it simply) code/component layer that glues other two together.
That way we can easily swap the presentation of the game using Asset Bundles in runtime.